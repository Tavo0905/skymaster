# Analizador de SkyMaster

from explorador import TipoDeComponente, Componente_Lexico
from arbol import ASA, Nodo, TipoNodo


class Analizador:
    cantidad_componentes: int
    componentes_lexicos: list

    posicion_componente_actual: int
    componente_actual: Componente_Lexico

    # Nuevo
    componente_actual: Componente_Lexico
    posicion_componente_anterior: int

    def __init__(self, listaDeComponentes):

        self.componentes_lexicos = listaDeComponentes
        self.cantidad_componentes = len(listaDeComponentes)

        self.posicion_componente_actual = 0
        self.componente_actual = listaDeComponentes[0]

        self.posicion_componente_anterior = 0
        self.componente_anterior = listaDeComponentes[0]

        self.asa = ASA()

    def imprimir_asa(self):
        # Imprime el arbol de sintaxis abstracta en preorden (ASA)
        print ("*********************")
        print ("________ARBOL________")
        print ("*********************")
        if self.asa.raiz is None:
            print([])
        else:
            
            self.asa.imprimir_preorden()

    def analizar(self):

        # Método principal que inicializa el análisis siguiendo el análisis por descenso recursivo

        self.asa.raiz = self.__analizar_programa()

    def __verificar(self, texto_esperado):

        # Verifica si el texto del componente léxico actual corresponde
        # con el esperado (el que se pasa por argumento)

        if self.componente_actual.texto != texto_esperado:
            print(f"ERROR EN LA LINEA: {self.componente_actual.lineaCodigo}, {self.componente_actual.texto} NO ES EL SIMBOLO ESPERADO, SE ESPERABA UN {texto_esperado}")
          

        self.__siguiente_componente()

    def __siguiente_componente(self):

        # Metodo que pasa al siguiente componente léxico
        # Esto revienta por ahora (Lo puso el profe esperemos que no)

        self.posicion_componente_actual += 1

        if self.posicion_componente_actual >= self.cantidad_componentes:
            self.cantidad_componentes = 0  # Porque en el while esta es la condicion de parada
            return

        self.componente_actual = \
            self.componentes_lexicos[self.posicion_componente_actual]
      

    def __analizar_programa(self):

        # Lenguaje ::= (Instruccion)*

        nodos_nuevos = []

        # pueden venir múltiples asignaciones o funciones
        while (True):
            
        

            if self.cantidad_componentes == 0:
                break

            # Si es función
            elif (self.componente_actual.texto == 'atraccion'):
                nodos_nuevos += [self.__analizar_función()]

            # Si es una bifurcacion (CONDICION)
            elif self.componente_actual.tipo == TipoDeComponente.CONDICION:
                nodos_nuevos += [self.__analizar_bifurcacion()]

            # Si es una asignacion
            elif self.componente_actual.tipo == TipoDeComponente.VARIABLE:
                nodos_nuevos += [self.__analizar_asignacion()]


            # Si es una estructura de repeticion
            elif self.componente_actual.tipo == TipoDeComponente.REPETICION:
                nodos_nuevos += [self.__analizar_repeticion()]

            # Aqui iria uno para los comentarios, pero de momento
            # los vamos a ignorar

            # Si es un print
            elif self.componente_actual.texto == 'boleteria':
                # esta funcion hay que crearla
                nodos_nuevos += [self.__analizar_print()]
                
            elif self.componente_actual.texto == "suerte":
                nodos_nuevos += [self.__analizar_random()]

            else:
                break

        # # De fijo al final una función principal
        # if (self.componente_actual.texto in ['jefe', 'jefa']):
        #     nodos_nuevos += [self.__analizar_principal()]
        # else:
        #     raise SyntaxError(str(self.componente_actual))

        "ESTO COMENTADO SE SUPONE QUE A NUESTRO LENGUAJE NADA QUE VER"

        return Nodo(TipoNodo.LENGUAJE, nodos=nodos_nuevos)

# HASTA AQUI LO DEJE PLANTEADO, EL RESTO ES MODIFICAR LAS FUNCIONES DE __analizar_x  GUIANDOSE CON EL EBNF (Dayron)

    def __analizar_asignacion(self):

        # Asignación ::= Identificador metale ( Literal | ExpresiónMatemática | (Invocación | Identificador) )
        # Asignacion ::= Variable ‘=’ Expresion

        self.componente_anterior = self.componente_actual
        self.posicion_componente_anterior = self.posicion_componente_actual

        nodos_nuevos = []

        # El identificador(CON 🎢) en esta posición es obligatorio
        nodos_nuevos += [self.__verificar_variable()]

        # Una asignación tipo: variable += numero / variable / invocacion
        if self.componente_actual.tipo == TipoDeComponente.OPERADOR:
            self.componente_actual = self.componente_anterior
            self.posicion_componente_actual = self.posicion_componente_anterior
            nodos_nuevos += [self.__analizar_expresión_matemática()]
            return Nodo(TipoNodo.ASIGNACIÓN, nodos=nodos_nuevos)

        # El igual es obligatorio
        self.__verificar('=')

        # Si el siguiente componente es un operador, entonces sería una expresion matematica
        if self.__componente_venidero().tipo == TipoDeComponente.OPERADOR:
            nodos_nuevos += [self.__analizar_expresión_matemática()]
            return Nodo(TipoNodo.ASIGNACIÓN, nodos=nodos_nuevos)

        # Lo que va despues del =

        # Si es un valor
        if self.componente_actual.tipo in [TipoDeComponente.ENTERO,
                                           TipoDeComponente.FLOTANTE,
                                           TipoDeComponente.VALOR_VERDAD,
                                           TipoDeComponente.TEXTO]:
            nodos_nuevos += [self.__analizar_tipoDato()]

        # Si es expresion
        # elif self.componente_actual.tipo == TipoDeComponente.:       "DE MOMENTO NOSE HACERLA"
            # nodos_nuevos += [self.__analizar_expresión_matemática()]

        # Tengo problemas con este, cuando quiero que la expresion matematica inicie con una invocacion
        elif self.componente_actual.tipo == TipoDeComponente.IDENTIFICADOR:
            nodos_nuevos += [self.__analizar_invocacion()]

        # Si es una variable
        elif self.componente_actual.tipo == TipoDeComponente.VARIABLE:
            nodos_nuevos += [self.__verificar_variable()]
        else:
           print (f"hay un error en esta linea: {self.componente_actual.lineaCodigo} \nLa asignacion es incorrecta")
           return Nodo(TipoNodo.ERROR,contenido="error sintactico")
            

        return Nodo(TipoNodo.ASIGNACIÓN, nodos=nodos_nuevos)

    def __analizar_expresión_matemática(self):  # CAMBIADO
        """
        Expresion ::= Valor (Operador Valor)*, expresiones seguidas sin paréntesis 
        No hacer: (2+4)*7
        Esto sí: 2+4*7
        Fallan las que inician con invocación, en proceso de corrección xq no encuentro el error :c
        """
        nodos_nuevos = []
        nodos_nuevos += [self.__analizar_expresión()]

        # Porque pueden ser varias operaciones seguidas
        while self.componente_actual.texto in ['✖', "➗", "➕", "➖", "✖", "%"]:

            nodos_nuevos += [self.__verificar_operador()]

            if self.componente_actual.texto == '=':
                self.__verificar('=')
            elif self.componente_actual.texto == '✖' or self.componente_actual.texto == "➗":
                nodos_nuevos += [self.__verificar_operador()]

            nodos_nuevos += [self.__analizar_expresión()]

        return Nodo(TipoNodo.EXPRESIÓN_MATEMÁTICA, nodos=nodos_nuevos)

    def __analizar_expresión(self):  # CAMBIADO

        if self.componente_actual.tipo == TipoDeComponente.ENTERO:

            return self.__verificar_entero()

        elif self.componente_actual.tipo == TipoDeComponente.VARIABLE:

            return self.__verificar_variable()

        elif self.componente_actual.tipo == TipoDeComponente.FLOTANTE:

            return self.__verificar_flotante()
        
        elif self.componente_actual.tipo == TipoDeComponente.TEXTO:
            print(f"\nEl tipo esperado: Variable, Número o invocación no coincide con {self.componente_actual.tipo} en la línea {self.componente_actual.lineaCodigo}\n")
            return self.__verificar_texto()
        
        elif self.componente_actual.tipo == TipoDeComponente.VALOR_VERDAD:

            return self.__verificar_valor_verdad()
        
        elif self.componente_actual.texto == "suerte":
            
            return self.__analizar_random()
        
        else:
            return self.__analizar_invocacion()

    def __analizar_función(self):  # TAVO
        """
        La estructura que le corresponde según la gramática es la siguiente:

        Funcion ::= ‘atraccion’ Identificador ‘(’ (Variable (‘,’ Variable)*)? ‘):’
                        (Instruccion)*

        """

        nodos_nuevos = []

        self.__verificar('atraccion')

        nodos_nuevos += [self.__verificar_identificador()]
        self.__verificar('(')
        # Si no hay parametros:
        if self.componente_actual.texto != ')':
            # Tiene la misma esturcura que los de invocacion
            # Reconsiderar el nombre de la funcion
            nodos_nuevos += [self.__analizar_parametrosDeinvocacion()]
        else:

            nodos_nuevos += [Nodo(TipoNodo.PARÁMETROS_FUNCIÓN, nodos=[Nodo(TipoNodo.VACIO,
                    contenido="sin parametros")])]
            
        self.__verificar(')')
        
        nodos_nuevos += [self.__analizar_bloque_instrucciones_funcion()]

        nodos_nuevos += [self.__analizar_retorno()]

        self.__verificar(';')

        # La función lleva el nombre del identificador
        return Nodo(TipoNodo.FUNCIÓN,
                    contenido=nodos_nuevos[0].contenido, nodos=nodos_nuevos)

    def __analizar_invocacion(self):  # CAMBIADO
        """
        Invocacion ::= Identificador‘(’ (Variable (‘,’ Variable)*)? ‘)’
        """
        # Invocacion ::= Identificador‘(’ (Variable (‘,’ Variable)*)? ‘)’
        nodos_nuevos = []

        # La invocacion debe seguir esta estructura
        nodos_nuevos += [self.__verificar_identificador()]
        self.__verificar('(')
        # Si no hay parametros:
        if self.componente_actual.texto == ')':
            self.__verificar(')')
        else:
            nodos_nuevos += [self.__analizar_parametrosDeinvocacion()]
            self.__verificar(')')

        return Nodo(TipoNodo.INVOCACIÓN, nodos=nodos_nuevos)

    def __analizar_parametrosDeinvocacion(self):  # CAMBIADO

        # Parámetros ::= (Variable| TipoDato | Invocación)? (‘,’ (Variable| TipoDato | Invocación ))*

        nodos_nuevos = []

        # Como ya se valido si no habia ningun parametro entonces obligatoriamente debe haber algo:

        # Si es variable
        if self.componente_actual.tipo == TipoDeComponente.VARIABLE:
            nodos_nuevos += [self.__verificar_variable()]
        # Si es una invocacion
        elif self.componente_actual.tipo == TipoDeComponente.IDENTIFICADOR:
            nodos_nuevos += [self.__analizar_invocacion()]
        # si no es ninguno de esos dos entonces a fuerza es un TipoDato
        else:
            nodos_nuevos += [self.__analizar_tipoDato()]

        while (self.componente_actual.texto == ','):
            self.__verificar(',')
            nodos_nuevos += [self.__analizar_parametrosDeinvocacion()]

        # Esto funciona con lógica al verrís... Si no revienta con error asumimos que todo bien y seguimos. #ESTO ES DEL PROFE

        return Nodo(TipoNodo.PARÁMETROS_FUNCIÓN, nodos=nodos_nuevos)

    def __analizar_instrucción(self):  # EN PROCESO (TAVO)
        """
        Tiene una estructura similar a la planteada en el inicio, al ejecutar el programa.
        Ya que las instrucciones son parte de la etapa inicial del código creado con el lenguaje,
        entonces se realizan las mismas validaciones.

        La estructura que sigue es la siguiente:

        Instruccion ::= Asignacion | Expresion | Funcion | Retorno | Bifurcacion | Repeticion | Comentario | Print

        En este caso algunos vienen implícitos en otras funciones, por lo que se requiere validar las que no lo están.
        Los comentarios son omitidos.
        """

        nodos_nuevos = []

        # Acá todo con if por que son opcionales
        if self.componente_actual.tipo == TipoDeComponente.VARIABLE:
            nodos_nuevos += [self.__analizar_asignacion()]
        
        elif self.componente_actual.texto == 'disco':
            nodos_nuevos += [self.__analizar_retorno()]

        elif self.componente_actual.texto == 'suerte':
            nodos_nuevos += [self.__analizar_random()]

        elif self.componente_actual.tipo == TipoDeComponente.IDENTIFICADOR:
            nodos_nuevos += [self.__analizar_invocacion()]

        elif self.componente_actual.tipo == TipoDeComponente.CONDICION:
            nodos_nuevos += [self.__analizar_bifurcacion()]

        elif self.componente_actual.tipo == TipoDeComponente.REPETICION:
            nodos_nuevos += [self.__analizar_repeticion()]

        elif self.componente_actual.texto == 'boleteria':
            nodos_nuevos += [self.__analizar_print()]
            
        elif self.componente_actual.tipo == TipoDeComponente.FINREPETICION:
            nodos_nuevos += [self.__analizar_fin_repeticion()]

        

        else:
            print(f"Hay un error en esta línea: {self.componente_actual.lineaCodigo} en la instrucción: {self.componente_actual.texto}")
            
            self.__siguiente_componente()
            raise Exception(f"Problablemente hace falta un disco (return) por la linea: {self.componente_actual.lineaCodigo}")
            #return Nodo(TipoNodo.ERROR,contenido="Error")

        # Acá yo debería volarme el nivel Intrucción por que no aporta nada
        return Nodo(TipoNodo.INSTRUCCIÓN, nodos=nodos_nuevos)

    # todavía no corre bien
    def __analizar_repeticion(self): # MAU
        """
        Repeticion ::= ‘boomerang’ Condicion ‘:’
			(Instruccion)*
			(FinRepeticion)?

        |   ‘pacuare’ Variable ‘balsa’ Variable ‘:’
			(Instruccion)*
			(FinRepeticion)?

        """
        nodos_nuevos = []
        
        aux = self.componente_actual.texto
        
        if self.componente_actual.texto == 'boomerang':
            self.__verificar('boomerang')
            nodos_nuevos += [self.__analizar_condición()]
        elif self.componente_actual.texto == 'pacuare':
            self.__verificar('pacuare')
            nodos_nuevos += [self.__verificar_variable()]
            self.__verificar('balsa')
            nodos_nuevos += [self.__verificar_variable()]

        nodos_nuevos += [self.__analizar_bloque_instrucciones()]
        if (self.componente_actual.tipo == TipoDeComponente.FINREPETICION):
            nodos_nuevos += [self.__analizar_fin_repeticion()]

        return Nodo(TipoNodo.REPETICIÓN, contenido = aux, nodos=nodos_nuevos)
    
    def __analizar_fin_repeticion(self):
        """
        FinRepeticion ::= ‘break’ | ‘continue’
        """
        nodos_nuevos = []

        if self.componente_actual.texto == 'break':
            self.__verificar('break')
        elif self.componente_actual.texto == 'continue':
            self.__verificar('continue')


        return Nodo(TipoNodo.FINREPETICION, contenido = "break")

    def __analizar_bifurcacion(self): # MAU
        """
        Bifurcacion ::= 'pase especial' Condicion ':'
		       	(Instruccion)*
		    ('tiquete' Condicion  ':'
		       	(Instruccion)* )*
		    ('parqueo:'
		       	(Instruccion)* )?
        """
        nodos_nuevos = []

        # debe empezar por un if (paseEspecial)
        if self.componente_actual.texto == 'paseEspecial':
            self.__verificar('paseEspecial')
            nodos_nuevos += [self.__analizar_condición()]
            nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        # pueden ir n elifs (tiquete)
        while self.componente_actual.texto == 'tiquete':
            self.__verificar('tiquete')
            nodos_nuevos += [self.__analizar_condición()]
            nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        # puede terminar con un else (parqueo)
        if self.componente_actual.texto == 'parqueo':
            self.__verificar('parqueo')
            nodos_nuevos += [self.__analizar_bloque_instrucciones()]

        return Nodo(TipoNodo.BIFURCACIÓN, nodos=nodos_nuevos)

    def __analizar_condición(self):# MAU
        """
        Condicion ::= (‘🚫’)? Valor ((Condicional | OperadorLogico) Condicion)*
        """
        nodos_nuevos = []

        # puede empezar por un not (🚫)
        if self.componente_actual.texto == '🚫':
            self.__verificar('🚫')

        # debe ir un valor
        nodos_nuevos += [self.__analizar_valor()]

        # pueden ir condicionales u operadores lógicos
        while self.componente_actual.tipo is TipoDeComponente.CONDICIONAL \
            or self.componente_actual.tipo is TipoDeComponente.OPERADOR:
            if self.componente_actual.tipo is TipoDeComponente.CONDICIONAL:
                nodos_nuevos += [self.__verificar_condicional()]
            elif self.componente_actual.tipo is TipoDeComponente.OPERADOR:
                nodos_nuevos += [self.__verificar_operador()]

            # ambos deben ir seguidos por una condición
           # print('CONDICION')
            nodos_nuevos += [self.__analizar_condición()]

        # puede ir seguido por varias condicionales
        # while self.componente_actual.tipo is TipoDeComponente.CONDICION:
        #     nodos_nuevos += [self.__analizar_bifurcacion()]

        return Nodo(TipoNodo.CONDICIONAL, nodos=nodos_nuevos)

    def __analizar_valor(self):
        """
        Valor ::= (TipoDato | Variable)
        """
        # Acá voy a cambiar el esquema de trabajo y voy a elminar algunos
        # niveles del árbol

        # El uno o el otro
        if self.componente_actual.tipo is TipoDeComponente.VARIABLE:
            nodo = self.__verificar_variable()
        else:
            nodo = self.__analizar_tipoDato()

        return nodo

    def __analizar_retorno(self):
        """
        Retorno :: disco (Valor)?
        """
        nodos_nuevos = []

        self.__verificar('disco')

        self.__verificar('(')

                
        # Este hay que validarlo para evitar el error en caso de que no
        # aparezca
        if self.componente_actual.tipo in [TipoDeComponente.ENTERO, TipoDeComponente.FLOTANTE, TipoDeComponente.VALOR_VERDAD, TipoDeComponente.TEXTO]:
            nodos_nuevos += [self.__analizar_valor()]
        elif self.componente_actual.tipo == TipoDeComponente.VARIABLE:
            nodos_nuevos += [self.__verificar_variable()]
        elif self.componente_actual.tipo == TipoDeComponente.IDENTIFICADOR:
            nodos_nuevos += [self.__analizar_invocacion()]
        # Sino todo bien...
        self.__verificar(')')
        
        return Nodo(TipoNodo.RETORNO, nodos=nodos_nuevos)

    def __analizar_tipoDato(self):

        # TipoDato ::= String | Integer | Decimal | Booleano

        # Si es string
        if self.componente_actual.tipo is TipoDeComponente.TEXTO:
            nodo = self.__verificar_texto()
        # Si es Integer
        elif self.componente_actual.tipo == TipoDeComponente.ENTERO or self.componente_actual.tipo == TipoDeComponente.FLOTANTE:
            nodo = self.__analizar_número()

        # Si es Booleano
        elif self.componente_actual.tipo is TipoDeComponente.VALOR_VERDAD:
            nodo = self.__verificar_valor_verdad()

        else:
            print(f'hay un error en esta linea: {self.componente_actual.lineaCodigo} deberia haber un entero, flotante, string o valor de verdad escrito de manera correcta')
            return Nodo(TipoNodo.ERROR,contenido="error sintactico")
        return nodo

    def __analizar_número(self):

        # Número ::= (Entero | Flotante)

        if self.componente_actual.tipo == TipoDeComponente.ENTERO:
            nodo = self.__verificar_entero()
        else:
            nodo = self.__verificar_flotante()

        return nodo

    def __analizar_bloque_instrucciones(self):
        """
        Este es nuevo y me lo inventé para simplicicar un poco el código...
        correspondería actualizar la gramática.

        BloqueInstrucciones ::= { Instrucción+ }
        """
        nodos_nuevos = []

        # Obligatorio
        self.__verificar(':')

        # mínimo una
        nodos_nuevos += [self.__analizar_instrucción()]

        # Acá todo puede venir uno o más
        while self.componente_actual.texto != ";":
            nodos_nuevos += [self.__analizar_instrucción()]

        # Obligatorio (REVISAR PORQUE CREO QUE ESTE ES EL PRINCIPAL PROBLEMA)
        #NUEVA FORMA DE TERMINAR EL BLOQUE DE INSTRUCCIONES
        self.__verificar(';')

        return Nodo(TipoNodo.BLOQUE_INSTRUCCIONES, nodos=nodos_nuevos)


    def __analizar_bloque_instrucciones_funcion(self):
        """
        Este es nuevo y me lo inventé para simplicicar un poco el código...
        correspondería actualizar la gramática.

        BloqueInstrucciones ::= { Instrucción+ }
        """
        nodos_nuevos = []

        # Obligatorio
        self.__verificar(':')

        # mínimo una
        nodos_nuevos += [self.__analizar_instrucción()]

        # Acá todo puede venir uno o más
        while self.componente_actual.texto != "disco":
            nodos_nuevos += [self.__analizar_instrucción()]

        return Nodo(TipoNodo.BLOQUE_INSTRUCCIONES, nodos=nodos_nuevos)

    def __analizar_print(self): # Completo (TAVO)
        """Analiza la instrucción del print. Este tiene la siguiente estructura:
        
        Print ::= ‘boleteria(’ (Valor | Funcion | Expresion (‘,’ Valor | Funcion | Expresion)*)? ‘)’

        Retorna:
            Nodo: Contiene la información de cada token relevante para el
                  funcionamiento del explorador.
        """
        nodos_nuevos = []
        
        self.__verificar("boleteria")
        self.__verificar("(")
        while self.componente_actual.texto != ")":
            if self.componente_actual.tipo in [TipoDeComponente.VALOR_VERDAD,\
                                                TipoDeComponente.VARIABLE, TipoDeComponente.FLOTANTE,\
                                                TipoDeComponente.ENTERO]:
                nodos_nuevos += [self.__analizar_expresión_matemática()]
            #POR EL MOMENTO LOS COMENTE PORQUE LA EXPRESION MATEMATICA SE BUGUEA
            # elif self.componente_actual.tipo in [TipoDeComponente.FLOTANTE, TipoDeComponente.ENTERO]:
            #     nodos_nuevos += [self.__analizar_expresión_matemática]
            elif self.componente_actual.tipo == TipoDeComponente.TEXTO:
                nodos_nuevos += [self.__verificar_texto()]
            elif self.componente_actual.texto == ",":
                self.__verificar(",")
            else:
                # MANEJO DE ERROR
                print(f'Hay un error en esta linea: {self.componente_actual.lineaCodigo}, \
                    debe ser una variable o un tipo de dato.')
                return Nodo(TipoNodo.ERROR,contenido="error sintactico")
        self.__verificar(")")
        return Nodo(TipoNodo.INSTRUCCIÓN, contenido = "boleteria", nodos = nodos_nuevos)

    def __analizar_random(self):
        """Analiza la funcion para obtener un numero aleatorio"""
        
        nodos_nuevos = []
            
        self.__verificar("suerte")
        self.__verificar("(")
        nodos_nuevos += [self.__verificar_entero()]
        self.__verificar(",")
        nodos_nuevos += [self.__verificar_entero()]
        self.__verificar(")")
        return Nodo(TipoNodo.INSTRUCCIÓN, contenido = "suerte", nodos = nodos_nuevos)


# Todos estos verificar se pueden unificar =*=
    def __verificar_operador(self):
        """
        Operador ::= (➕|➖|✖|➗|✖✖|➗➗|%)
        
        """
        self.__verificar_tipo_componente(TipoDeComponente.OPERADOR)

        nodo = Nodo(TipoNodo.OPERADOR, contenido=self.componente_actual.texto)
        self.__siguiente_componente()

        return nodo

    def __verificar_valor_verdad(self):
        """
        ValorVerdad ::= (True | False)
        """
        self.__verificar_tipo_componente(TipoDeComponente.VALOR_VERDAD)

        nodo = Nodo(TipoNodo.VALOR_VERDAD,
                    contenido=self.componente_actual.texto)
        self.__siguiente_componente()
        return nodo

    def __verificar_condicional(self): # MAU
        """
        Condicional ::= (==|🚫|<(=)?|>(=)?)
        """
        self.__verificar_tipo_componente(TipoDeComponente.CONDICIONAL)

        nodo = Nodo(TipoNodo.CONDICIONAL,
                    contenido=self.componente_actual.texto)
        self.__siguiente_componente()
        return nodo

    def __verificar_texto(self):
        """
        Verifica si el tipo del componente léxico actuales de tipo TEXTO

        Texto ::= 👽([a-zA-Z0-9ÑñÁáÉéÍíÓóÚú!@#$%^&*()\[\]{}\-_+=<>?/\\|\s+]*)👽
        """
        self.__verificar_tipo_componente(TipoDeComponente.TEXTO)

        nodo = Nodo(TipoNodo.TEXTO, contenido=self.componente_actual.texto)
        self.__siguiente_componente()
        return nodo

    def __verificar_entero(self):
        """
        Entero ::= -?[0-9]+
        """
        self.__verificar_tipo_componente(TipoDeComponente.ENTERO)

        nodo = Nodo(TipoNodo.ENTERO, contenido=self.componente_actual.texto)

        self.__siguiente_componente()
        return nodo

    def __verificar_flotante(self): 
        """
        Flotante ::= -?[0-9]+\.[0-9]+
        """
        self.__verificar_tipo_componente(TipoDeComponente.FLOTANTE)

        nodo = Nodo(TipoNodo.FLOTANTE, contenido=self.componente_actual.texto)
        self.__siguiente_componente()
        return nodo

    def __verificar_identificador(self):
        """
        Identificador ::= [a-z][a-zA-Z0-9]* 
        """
        self.__verificar_tipo_componente(TipoDeComponente.IDENTIFICADOR)

        nodo = Nodo(TipoNodo.IDENTIFICADOR,
                    contenido=self.componente_actual.texto)
        self.__siguiente_componente()
        return nodo

    def __verificar_variable(self):
        """
        Verifica si el tipo del componente léxico actual es de tipo VARIABLE
        Variable ::= ‘🎢’Identificador
        """
        self.__verificar_tipo_componente(TipoDeComponente.VARIABLE)

        nodo = Nodo(TipoNodo.VARIABLE, contenido=self.componente_actual.texto)
        self.__siguiente_componente()
        return nodo

    def __verificar_tipo_componente(self, tipo_esperado):

        # Verifica un componente por tipo... no hace mucho pero es para
        # centralizar el manejo de errores #ESTO ES DEL PROFE

        if self.componente_actual.tipo is not tipo_esperado:
        
            print(f"\nEl tipo esperado: {tipo_esperado} no coincide con {self.componente_actual.tipo} en la linea {self.componente_actual.lineaCodigo}\n")

    def __componente_venidero(self, avance=1):
        """
        Retorna el componente léxico que está 'avance' posiciones más
        adelante... por default el siguiente. Esto sin adelantar el
        contador del componente actual.
        """
        return self.componentes_lexicos[self.posicion_componente_actual+avance]


