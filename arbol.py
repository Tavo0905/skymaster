# Clases para el manejo de un árbol de sintáxis abstracta

from enum import Enum, auto

class TipoNodo(Enum):
    
    #Los tipos de nodo del arbol
    LENGUAJE = auto()
    ASIGNACIÓN            = auto()
    VARIABLE              = auto()
    EXPRESIÓN_MATEMÁTICA  = auto()
    EXPRESIÓN             = auto()
    FUNCIÓN               = auto()
    INVOCACIÓN            = auto()
    PARÁMETROS_FUNCIÓN    = auto()
    PARÁMETROS_INVOCACIÓN = auto()
    INSTRUCCIÓN           = auto()
    REPETICIÓN            = auto()
    BIFURCACIÓN           = auto()
    OPERADOR_LÓGICO       = auto()
    CONDICIÓN             = auto()
    CONDICIONAL           = auto()
    COMPARACIÓN           = auto()
    RETORNO               = auto()
    ERROR                 = auto()
    PRINCIPAL             = auto()
    BLOQUE_INSTRUCCIONES  = auto()
    OPERADOR              = auto()
    VALOR_VERDAD          = auto()
    TEXTO                 = auto()
    ENTERO                = auto()
    FLOTANTE              = auto()
    VACIO                 = auto()
    IDENTIFICADOR         = auto()
    FINREPETICION         = auto()


import copy

class Nodo:

    tipo      : TipoNodo
    contenido : str
    atributos : dict

    def __init__(self, tipo, contenido = None, nodos = [], atributos = {}):

        self.tipo      = tipo
        self.contenido = contenido
        self.nodos     = nodos
        self.atributos = copy.deepcopy(atributos)

    def visitar(self, visitador):
        return visitador.visitar(self)


    def __str__(self):

        # Coloca la información del nodo
        resultado = '{:30}\t'.format(self.tipo)
        
        if self.contenido is not None:
            resultado += '{:10}\t'.format(self.contenido)
        else:
            resultado += '{:10}\t'.format('')


        if self.atributos != {}:
            resultado += '{:38}'.format(str(self.atributos))
        else:
            resultado += '{:38}\t'.format('')

        if self.nodos != []:
            resultado += '<'

            
            for nodo in self.nodos[:-1]:
                if nodo is not None:
                    resultado += '{},'.format(nodo.tipo) #Imprime los nodos del siguiente nivel

            resultado += '{}'.format(self.nodos[-1].tipo)
            resultado += '>'

        return resultado


class ASA:

    raiz : Nodo

    def imprimir_preorden(self):
        self.__preorden(self.raiz)

    def __preorden(self, nodo):

        if nodo is not None:
            print(nodo)
            for nodo in nodo.nodos:
                self.__preorden(nodo)
            
            
