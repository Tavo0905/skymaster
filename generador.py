# Implementa el veficador de ciruelas

from arbol import ASA, Nodo, TipoNodo
from visitadores import VisitantePython

class Generador:

    asa            : ASA
    visitador      : VisitantePython

    ambiente_estandar = """import sys
import random
"""

    def __init__(self, nuevo_asa: ASA):

        self.asa            = nuevo_asa
        self.visitador      = VisitantePython() 

    def imprimir_asa(self):
        """
        Imprime el árbol de sintáxis abstracta
        """
            
        if self.asa.raiz is None:
            print([])
        else:
            self.asa.imprimir_preorden()

    def generar(self):
        resultado = self.visitador.visitar(self.asa.raiz)
        #print(self.ambiente_estandar)
        #print(resultado)
        return self.ambiente_estandar + resultado


