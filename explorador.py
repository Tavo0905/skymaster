# Explorador (scanner) del lenguaje SkyMaster
from enum import Enum, auto
import re

class TipoDeComponente(Enum):
   
    #Enum que contiene los tipos de componentes existentes
    #Esta clase sirve como auxiliar para la validacion

    COMENTARIO = auto()
    REPETICION = auto()
    CONDICION = auto()
    ASIGNACION = auto()
    OPERADOR = auto()
    CONDICIONAL = auto() # CONDICIONAL
    VALOR_VERDAD = auto()
    ENTERO = auto()
    FLOTANTE = auto()
    VARIABLE = auto()
    TEXTO = auto()
    IDENTIFICADOR = auto()
    PUNTUACION = auto()
    BLANCOS = auto()
    PALABRA_CLAVE = auto()
    FUNCION = auto()
    PRINT = auto()
    NINGUNO = auto()
    INDICE = auto() #sirve para las listas [], EJEMPLO: Lista[i] es para hacer match con esos corchetes "[]"
    FINREPETICION = auto()

class Componente_Lexico:

    #Esta clase sirve para guardar la info o estructura de un componente léxico
    #Guarda la línea de código en el que está el componente léxico
    
    tipo: TipoDeComponente
    lineaCodigo: int
    texto: str

    def __init__(self, tipoEntrada: TipoDeComponente, nuevaLineaCodigo: int, textoNuevo: str):
        self.tipo = tipoEntrada
        self.texto = textoNuevo
        self.lineaCodigo = nuevaLineaCodigo

    def __str__(self):
        resultado = f'{self.tipo:30} <{self.texto}> <Linea de codigo: {self.lineaCodigo}>'
        return resultado


class Explorador:
    descriptoresComponentes = [
        (TipoDeComponente.COMENTARIO, r'^📢.*📢|🗣.*'),
        (TipoDeComponente.PALABRA_CLAVE, r'^(atraccion|disco|boleteria|suerte)'),
        (TipoDeComponente.CONDICION, r'^(paseEspecial|tiquete|parqueo)'),
        (TipoDeComponente.REPETICION, r'^(boomerang|pacuare)'),
        (TipoDeComponente.FINREPETICION, r'^(break|continue)'),
        (TipoDeComponente.VARIABLE, r'^🎢([a-z]([a-zA-z0-9_])*)'),
        (TipoDeComponente.CONDICIONAL, r'^(==|🚫|<(=)?|>(=)?)'),
        (TipoDeComponente.ASIGNACION, r'^='),
        (TipoDeComponente.INDICE, r'^\[|\]'),
        (TipoDeComponente.OPERADOR, r'^(➕|➖|✖|➗|✖✖|➗➗|%|⛔)'),
        (TipoDeComponente.TEXTO, r'^👽([a-zA-Z0-9ÑñÁáÉéÍíÓóÚú!@ÿ#$%^&*.()\[\]{}\-_+=<>?/\\|\s+]*)👽'),
        (TipoDeComponente.IDENTIFICADOR, r'^([a-z]([a-zA-z0-9_])*)'),
        (TipoDeComponente.ENTERO, r'^(-?[0-9]+)'),
        (TipoDeComponente.FLOTANTE, r'^(-?[0-9]+\.[0-9]+)'),
        (TipoDeComponente.VALOR_VERDAD, r'^(True|False)'),
        (TipoDeComponente.PUNTUACION, r'^([/{}:;,()])'),
        (TipoDeComponente.BLANCOS, r'^\s+')
    ]
    hay_error = False

    def __init__(self, contenidoDeArchivo):
        self.texto = contenidoDeArchivo
        self.componentes = []

    def exploracion(self):
   
        #Itera cada línea y genera los componentes lexicos
        numeroDeLinea = 0
        
        for linea in self.texto:
            resultado = self.explorar_linea(linea,numeroDeLinea)

            if (self.hay_error == True):
                break
            
            self.componentes = self.componentes + resultado
            numeroDeLinea += 1


    def imprimir_componentes(self):

        #Imprime en formato legible los componentes léxicos creados
 
        for componente in self.componentes:
            print(componente)


    def explorar_linea(self, linea, numeroDeLinea):
     
        #Va por cada linea y extrae los componentes léxicos

        componentes = []
        
        # Toma una línea y la va reduciendo hasta que se acaba
        while(linea !=  ""):

            # Separa las estructuras de componente en dos variables (Porque eran tuplas)
            alguna_coincidencia = False
            for tipoComponente, regex in self.descriptoresComponentes:
    
                # Trata de hacer match con la estructura (tupla) actual
               
                respuesta = re.match(regex, linea)  

                # Si encuentra coincidencia se pasa a generar el componente léxico 
                if respuesta is not None :

                    # si la coincidencia corresponde a un BLANCO o un
                    # COMENTARIO se ignora 
                    alguna_coincidencia = True
                    if tipoComponente is not TipoDeComponente.BLANCOS and \
                       tipoComponente is not TipoDeComponente.COMENTARIO:
                        nuevaLineaCodigo = numeroDeLinea + 1
                        #Se crea el componente léxico y se guarda guarda
                        nuevo_componente = Componente_Lexico(tipoComponente, nuevaLineaCodigo, respuesta.group())
                        componentes.append(nuevo_componente)

                    # Se elimina el pedazo que ya hizo match
                    #print ("antes de eliminar: ", tipoComponente)
                    linea = linea[respuesta.end():]

                    #print ("despues de eliminar: ", linea)
                    break

            if not alguna_coincidencia:
                print("error en la linea:", numeroDeLinea, "en:")
                print(linea)
                self.hay_error = True
                return componentes

        return componentes

