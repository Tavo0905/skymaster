# SkyMaster

## Descripción de archivos
- explorador.py tiene las clases necesarias para identificar los componentes léxicos

- prueba.py, prueba_dos.py y prueba_tres.py tienen código escrito en SkyMaster

- README.md contiene información de interés

- SkyMaster.py es un transpilador para el código


## Instrucciones para correr el transpilador
Se debe abrir la carpeta del proyecto en la consola y correr el siguiente comando donde nombreArchivo es el nombre del archivo que contiene el código que se quiere probar.

python skymaster.py --solo-explorar [nombreArchivo]