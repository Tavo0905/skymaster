from explorador import Explorador
from analizador import Analizador
from generador import Generador
from verificador import *
import argparse, os

parser = argparse.ArgumentParser(description = 'Transpilador para lenguaje SkyMaster')

# Recibe los argumentos o parametros que fueron otorgados en consola

# Detecta el Explorador
parser.add_argument('--solo-explorar', dest='explorador', action='store_true', 
        help='ejecuta solo el explorador y retorna una lista de componentes léxicos')

# Detecta el Analizador
parser.add_argument('--solo-analizar', dest='analizador', action='store_true', 
        help='ejecuta solo el analizador y retorna un arbol de sintaxis abstracta con los componentes procesados')

parser.add_argument('--solo-verificar', dest='verificador', action='store_true', 
        help='''ejecuta hasta el verificador y retorna un preorden del árbol
        sintáctico y estructuras de apoyo generadas en la verificación''')

parser.add_argument('--solo-generar', dest='generador', action='store_true', 
        help='''ejecuta desde el explorador hasta el generador y retorna el código
        del archivo como un código de python''')

parser.add_argument("--ejecutar", dest = "ejecutar", action = "store_true",
                    help = "Ejecuta el transpilador completo y corre el código generado")


# Recibe el archivo a transpilar
parser.add_argument('archivo',
        help='Archivo de código fuente')

# Permite leer y cargar el archivo al codigo
def cargarArchivo(ruta):
    with open(ruta, 'r', encoding='utf-8') as archivo:
        return archivo.readlines()


def main():

    args = parser.parse_args()

    if args.explorador is True: 

        texto = cargarArchivo(args.archivo)

        exp = Explorador(texto)
        exp.exploracion()
        if (exp.hay_error == False):
            exp.imprimir_componentes()
            
    elif args.analizador is True:
        texto = cargarArchivo(args.archivo)

        exp = Explorador(texto)
        exp.exploracion()
        if (exp.hay_error == False):
            #exp.imprimir_componentes()
            analizador = Analizador(exp.componentes)
            analizador.analizar()
            analizador.imprimir_asa()

    elif args.verificador is True:
        texto = cargarArchivo(args.archivo)

        exp = Explorador(texto)
        exp.exploracion()
        if (exp.hay_error == False):
            #exp.imprimir_componentes()
            analizador = Analizador(exp.componentes)
            analizador.analizar()
            #analizador.imprimir_asa() 
            verificador = Verificador(analizador.asa)
            verificador.verificar()   
            verificador.imprimir_asa()

    elif args.generador is True:
        texto = cargarArchivo(args.archivo)

        exp = Explorador(texto)
        exp.exploracion()
        if (exp.hay_error == False):
            #exp.imprimir_componentes()
            analizador = Analizador(exp.componentes)
            analizador.analizar()
            #analizador.imprimir_asa() 
            verificador = Verificador(analizador.asa)
            verificador.verificar()   
            #verificador.imprimir_asa()
            generador = Generador(verificador.asa)
            print("\n\nGENERADOR\n\n")
            generador.generar()

    elif args.ejecutar is True:
        texto = cargarArchivo(args.archivo)

        exp = Explorador(texto)
        exp.exploracion()
        if (exp.hay_error == False):
            analizador = Analizador(exp.componentes)
            analizador.analizar() 
            verificador = Verificador(analizador.asa)
            verificador.verificar()   
            generador = Generador(verificador.asa)
            codigo = generador.generar()
            archivo = open("ejecutable.py", "w", encoding = "utf-8")
            archivo.write(codigo)
            archivo.close()
            os.system("cls")
            os.system("python ejecutable.py")
            

    else:
        parser.print_help()


if __name__ == '__main__':
    main()
